import './App.css';
import { useState } from 'react';
import JitsiMeetingComponent from './JitsiMeetingComponent';
import JaasMeetingComponent from './JaasMeetingComponent';
import MyCalendarComponent from './Components/CalanderComponent';

function App() {
  const [jistiMeetState, setJitsiMeetState] = useState(false);
  const [calancerState, setCalancerState] = useState(false);
  const [meetingLinks,setMeetingLinks] = useState([]);
  const [link,setLink] = useState('');

  const handleJitsiMeetState = () => {
      setJitsiMeetState(!jistiMeetState);
      setCalancerState(false);
   }
   const handleCalancerState = () => {
      setCalancerState(!calancerState);
      setJitsiMeetState(false);
   }

   const handleLinkClick = (link) => {
      setLink(link);
      setJitsiMeetState(true);
   }
  return (
    <div className="App" style={{height:'100vh'}}>
       <div>
          <p style={{cursor:'pointer'}} className='' onClick={handleJitsiMeetState}>For meeting Click here</p>
       </div>
       <div>
            <p style={{cursor:'pointer'}} className='' onClick={handleCalancerState}>For Calander Click here</p>
       </div>
       <div>
        {
         meetingLinks &&
            meetingLinks.map((link,index) => {
                  return <p style={{cursor:'pointer'}} key={index} onClick={()=>handleLinkClick(link)}>{link}</p>
               })
        }
       </div>
       <div style={{height:'400px'}}>
          {jistiMeetState &&  <JitsiMeetingComponent link={link}/>}
          {calancerState && <MyCalendarComponent meetingLinks={meetingLinks} setMeetingLinks={setMeetingLinks} />}
       </div>
    </div>
  );
}

export default App;
