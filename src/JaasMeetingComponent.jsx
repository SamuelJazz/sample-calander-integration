import { 
  // JitsiMeeting, 
  JaaSMeeting 
} from "@jitsi/react-sdk";

/*
 you can generate the APP_ID and VALID JWT from the above link in the note.
*/
const YOUR_APP_ID = `vpaas-magic-cookie-d752fb4b32f444b7aa30264798123b6d`;
const YOUR_VALID_JWT = `eyJraWQiOiJ2cGFhcy1tYWdpYy1jb29raWUtZDc1MmZiNGIzMmY0NDRiN2FhMzAyNjQ3OTgxMjNiNmQvZDZlOWVhLVNBTVBMRV9BUFAiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiJqaXRzaSIsImlzcyI6ImNoYXQiLCJpYXQiOjE2ODY4MDM2NTYsImV4cCI6MTY4NjgxMDg1NiwibmJmIjoxNjg2ODAzNjUxLCJzdWIiOiJ2cGFhcy1tYWdpYy1jb29raWUtZDc1MmZiNGIzMmY0NDRiN2FhMzAyNjQ3OTgxMjNiNmQiLCJjb250ZXh0Ijp7ImZlYXR1cmVzIjp7ImxpdmVzdHJlYW1pbmciOnRydWUsIm91dGJvdW5kLWNhbGwiOnRydWUsInNpcC1vdXRib3VuZC1jYWxsIjpmYWxzZSwidHJhbnNjcmlwdGlvbiI6dHJ1ZSwicmVjb3JkaW5nIjp0cnVlfSwidXNlciI6eyJoaWRkZW4tZnJvbS1yZWNvcmRlciI6ZmFsc2UsIm1vZGVyYXRvciI6dHJ1ZSwibmFtZSI6InNhbXVlbGphenpqb2huIiwiaWQiOiJnb29nbGUtb2F1dGgyfDEwNzYxNTIyMTA5MDQ3ODcxOTE2MSIsImF2YXRhciI6IiIsImVtYWlsIjoic2FtdWVsamF6empvaG5AZ21haWwuY29tIn19LCJyb29tIjoiKiJ9.YlBfMIBXefK3JcMDG1JgcVKI67MY9N1o6Okuh5DD-qkKyDOTg7HrbybBDY_jBk0vzNKoNsKNCi-bV89PYeOrzU0qN57hZQpni1ws8naD_yP5FhJbBjtaNtUtwha76zdCkXT0UoVen0Wq-l6IBxV17HQQDWuNsvRTX05ZC7ztjfS7xNHVBssxSX_fR38GdulxV6oXs9ZBXuCPul_2xWspD8VBUhoDDuljst5dKKiMDrDyu9-kbs4hcWO2wKC36GMLXAbdfWzXPARiI9SX8apHryNg9HYq8QmpZRlD77Y-nirO0i4GdpFOI7HdIbr7bPfjtIdQb4ymERlK3sUVXSyZIQ`;

function JaasMeetingComponent() {
  return <div className="jaas" style={{ height: '100vh',width:'80%' }}>
  <JaaSMeeting
    appId = { YOUR_APP_ID }
    roomName = "Jazz"
    jwt = { YOUR_VALID_JWT }
    configOverwrite = {{
      disableThirdPartyRequests: true,
      disableLocalVideoFlip: true,
      backgroundAlpha: 0.5  
    }}
    // interfaceConfigOverwrite = {{
    //     VIDEO_LAYOUT_FIT: 'nocrop',
    //     MOBILE_APP_PROMO: false,
    //     TILE_VIEW_MAX_COLUMNS: 4
    // }}
    // spinner = { SpinnerView }
    // onApiReady = { (externalApi) => { ... } }
  />

  </div>;
}

export default JaasMeetingComponent;
