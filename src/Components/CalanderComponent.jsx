import React, { useEffect, useState } from 'react';
import { gapi } from 'gapi-script';

const config = {
  clientId: '434602450672-hl37t2l87bi31ds97gk3e5d1n8dvucf6.apps.googleusercontent.com',
  apiKey: 'AIzaSyAwLrUsck8hfXRiaWJMBgwuvkBXl_K3XhU',
  scope: 'https://www.googleapis.com/auth/calendar',
  discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
};

const MyCalendarComponent = ({meetingLinks,setMeetingLinks}) => {
  const [events, setEvents] = useState({
    summary:'',
      end: '',
      attendees: '',
      conferenceData: '',
      roomName:'', 
});
  const [isSignedIn, setIsSignedIn] = useState(false);

  useEffect(() => {
    gapi.load('client:auth2', () => {
      gapi.client
        .init(config)
        .then(() => {
          const authInstance = gapi.auth2.getAuthInstance();
          setIsSignedIn(authInstance.isSignedIn.get());
        })
        .catch((error) => {
          console.log('Error initializing Google Calendar API:', error);
        });
    });
    if (isSignedIn) {
        gapi.client.calendar.events
          .list({
            calendarId: 'primary',
            timeMin: new Date().toISOString(),
            showDeleted: false,
            singleEvents: true,
            maxResults: 10,
            orderBy: 'startTime',
          })
          .then((response) => {
            const { items } = response.result;
            setEvents(items);
          })
          .catch((error) => {
            console.error('Error fetching calendar events:', error);
          });
      }
  }, []);

  function generateRandomRoomName(length) {
    const characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    let roomName = '';
    for (let i = 0; i < length; i++) {
      const randomIndex = Math.floor(Math.random() * characters.length);
      roomName += characters[randomIndex];
    }
    return roomName;
  }

  const handleSignIn = () => {
    gapi.auth2.getAuthInstance().signIn().then(() => {
      setIsSignedIn(true);
    });
  };

  const handleSignOut = () => {
    gapi.auth2.getAuthInstance().signOut().then(() => {
      setIsSignedIn(false);
    });
  };

  const handleCreateEvent = () => {
    const event = {
      summary: 'Video Call Meeting',
      start: {
        dateTime: '2023-06-17T10:00:00', // Set the meeting start time
        timeZone: 'Asia/Kolkata',
      },
      end: {
        dateTime: '2023-06-17T11:00:00', // Set the meeting end time
        timeZone: 'Asia/Kolkata',
      },
      attendees: [
        { email: 'samueljazzjohn@gmail.com' },
        { email: 'samuelj.bethel@gmail.com' },
        {email : 'samueljazzjohn1999@gmail.com'}
        // Add more participants' email addresses
      ],
      conferenceData: {
        createRequest: {
          requestId: 'your-unique-request-id',
          conferenceSolutionKey: {
            type: 'hangoutsMeet',
          },
        },
      },
      roomName: generateRandomRoomName(10), // Generate a random room name
    };

    gapi.client.calendar.events
      .insert({
        calendarId: 'primary', // Use the primary calendar or specify the calendar ID
        resource: event,
        sendUpdates: 'all',
      })
      .then((response) => {
        console.log('Event created:', response.result);
        const conferenceData = response.result.conferenceData;
        // const meetingLink = conferenceData && conferenceData.entryPoints[0].uri;
        const meetingLink = `https://join.jit.smarter.codes/${event.roomName}`
        if (meetingLink) {
          const participantEmails = event.attendees.map((attendee) => attendee.email).join(', ');
          const emailSubject = `Meeting Link for ${event.summary}`;
          const emailBody =  `Here is the meeting link for the event:\n\n${encodeURIComponent(meetingLink)}`;

          // Send email to participants
          // Use the Gmail API or your preferred email sending method to send emails
          sendEmailToParticipants(participantEmails, emailSubject, emailBody);
          setMeetingLinks([...meetingLinks, meetingLink]);
 
        }
      })
      .catch((error) => {
        console.error('Error creating event:', error);
      });
  };

  const sendEmailToParticipants = (emails, subject, body) => {
    console.log(emails, subject, body);
    // Implement your email sending logic here
    // You can use the Gmail API or any other method to send emails
    // Here's a simplified example using window.open to compose a new email
    const mailtoLink = `mailto:${emails}?subject=${
      subject
    }&body=${encodeURIComponent(body)}`;
    window.open(mailtoLink);
  };

//   useEffect(() => {
//     if (isSignedIn) {
//       gapi.client.calendar.events
//         .list({
//           calendarId: 'primary',
//           timeMin: new Date().toISOString(),
//           showDeleted: false,
//           singleEvents: true,
//           maxResults: 10,
//           orderBy: 'startTime',
//         })
//         .then((response) => {
//           const { items } = response.result;
//           setEvents(items);
//         })
//         .catch((error) => {
//           console.error('Error fetching calendar events:', error);
//         });
//     }
//   }, [isSignedIn]);

  return (
    <>
      {isSignedIn ? (
        <>
          <button style={{cursor:'pointer'}} onClick={handleSignOut}>Sign Out</button>
          <button style={{cursor:'pointer'}} onClick={handleCreateEvent}>Create Event</button>
          {/* <ul>
            {events.map((event) => (
              <li key={event.id}>{event.summary}</li>
            ))}
          </ul> */}
        </>
      ) : (
        <button style={{cursor:'pointer'}} onClick={handleSignIn}>Sign In</button>
      )}
    </>
  );
};

export default MyCalendarComponent;
