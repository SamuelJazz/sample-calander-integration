import React, { useEffect } from "react";
import { JitsiMeeting } from "@jitsi/react-sdk";



const JitsiMeetingComponent = ({link}) => {

  console.log("link", link);

  const [meetingDomain, setMeetingDomain] = React.useState();

  useEffect(() => {
    if (link) {
      const domain = link.replace(/^(https?:\/\/)?/, ""); // remove http(s)://
      setMeetingDomain(domain);
    }
      }, [link]);

  return (
    <>
    {
      link &&
      link ? <JitsiMeeting
      // domain={meetingDomain}
      domain="join.jit.smarter.codes/demo"
      roomName="jazz"
      configOverwrite={{
        startWithAudioMuted: true,
        disableModeratorIndicator: true,
        startScreenSharing: true,
        enableEmailInStats: false,
      }}
      interfaceConfigOverwrite={{
        DISABLE_JOIN_LEAVE_NOTIFICATIONS: true,
      }}
      userInfo={{
        displayName: "Jazz",
      }}
      onApiReady={(externalApi) => {
        console.log("onApiReady", externalApi);
        // here you can attach custom event listeners to the Jitsi Meet External API
        // you can also store it locally to execute commands
      }}
      getIFrameRef={(iframeRef) => {
        iframeRef.style.height = "800px";
      }}
    />:
    <JitsiMeeting
      domain="join.jit.smarter.codes/demo"
      roomName="jazz"
      configOverwrite={{
        startWithAudioMuted: true,
        disableModeratorIndicator: true,
        startScreenSharing: true,
        enableEmailInStats: false,
      }}
      interfaceConfigOverwrite={{
        DISABLE_JOIN_LEAVE_NOTIFICATIONS: true,
      }}
      userInfo={{
        displayName: "Jazz",
      }}
      onApiReady={(externalApi) => {
        console.log("onApiReady", externalApi);
        // here you can attach custom event listeners to the Jitsi Meet External API
        // you can also store it locally to execute commands
      }}
      getIFrameRef={(iframeRef) => {
        iframeRef.style.height = "800px";
      }}
    />
    }
    </>
  );
};

export default JitsiMeetingComponent;
